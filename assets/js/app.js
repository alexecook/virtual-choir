var VirtualChoir = function() {
	var audio,
		recorder,
		user = window.user,
		videos = {
			'female':[
				{
					'title' : 'Verse',
					'url' : '/prompterVideos/v2/female-verse-orange.mp4'
				},
				{
					'title' : 'Oh\'s Great Chant',
					'url' : '/prompterVideos/v2/female-chant-orange.mp4'
				},
				{
					'title' : 'Ohs Whows',
					'url' : '/prompterVideos/v2/female-ohs-orange.mp4'
				},
				{
					'title' : 'Change the World',
					'url' : '/prompterVideos/v2/female-change-the-world-orange.mp4'
				},
				{
					'title' : 'We Can',
					'url' : '/prompterVideos/v2/female-we-can-orange.mp4'
				},
				{
					'title' : 'Yes We Can Chant',
					'url' : '/prompterVideos/v2/female-yes-we-can-chant-orange.mp4'
				},
//				{
//					'title' : 'Chant Ohs (Harmony)',
//					'url' : '/prompterVideos/v2/female-harmony-chant-ohs-orange.mp4'
//				},
//				{
//					'title' : 'Change the World (Harmony)',
//					'url' : '/prompterVideos/v2/female-harmony-change-the-world-orange.mp4'
//				},
//				{
//					'title' : 'Ohs (Harmony)',
//					'url' : '/prompterVideos/v2/female-harmony-ohs-orange.mp4'
//				},
//				{
//					'title' : 'Verse (Harmony)',
//					'url' : '/prompterVideos/v2/female-harmony-chant-ohs-orange.mp4'
//				},
//				{
//					'title' : 'We Can (Harmony)',
//					'url' : '/prompterVideos/v2/female-harmony-we-can-orange.mp4'
//				},
				{
					'title' : 'Herbalife Long',
					'url' : '/prompterVideos/v2/male-and-female-herbalife-long.mp4'
				},
				{
					'title' : 'Herbalife Short',
					'url' : '/prompterVideos/v2/male-and-female-herbalife-short.mp4'
				}
			],
			'male':[
				
				{
					'title' : 'Verse',
					'url' : '/prompterVideos/v2/male-verse-orange.mp4'
				},
				{
					'title' : 'Oh\'s Great Chant',
					'url' : '/prompterVideos/v2/male-chant-orange.mp4'
				},
				{
					'title' : 'Ohs Whows',
					'url' : '/prompterVideos/v2/male-ohs-orange.mp4'
				},
				{
					'title' : 'Change the World',
					'url' : '/prompterVideos/v2/male-change-the-world-orange.mp4'
				},
				{
					'title' : 'We Can',
					'url' : '/prompterVideos/v2/male-we-can-orange.mp4'
				},
				{
					'title' : 'Yes We Can Chant',
					'url' : '/prompterVideos/v2/male-yes-we-can-chant-orange.mp4'
				},
//				{
//					'title' : 'Chant Ohs (Harmony)',
//					'url' : '/prompterVideos/v2/male-harmony-chant-ohs-orange.mp4'
//				},
//				{
//					'title' : 'Change the World (Harmony)',
//					'url' : '/prompterVideos/v2/male-harmony-change-the-world-orange.mp4'
//				},
//				{
//					'title' : 'Ohs (Harmony)',
//					'url' : '/prompterVideos/v2/male-harmony-ohs-orange.mp4'
//				},
//				{
//					'title' : 'Verse (Harmony)',
//					'url' : '/prompterVideos/v2/male-harmony-chant-ohs-orange.mp4'
//				},
//				{
//					'title' : 'We Can (Harmony)',
//					'url' : '/prompterVideos/v2/male-harmony-we-can-orange.mp4'
//				},
				{
					'title' : 'Herbalife Long',
					'url' : '/prompterVideos/v2/male-and-female-herbalife-long.mp4'
				},
				{
					'title' : 'Herbalife Short',
					'url' : '/prompterVideos/v2/male-and-female-herbalife-short.mp4'
				}
			]
		}, 
		selectedVideo = null,
		$container = $('.container'), 
		$recorder = $('ziggeo'), 
		zRecorder = ZiggeoApi.Embed.get('ziggeoPlayer'), 
		$prompter = $('#largeVideoPlayer'),
		$videosList = $('#videosList table');
	
	$recorder.css('visibility', 'hidden');
	
	$('#bottom .controls li.play button').on('click', function(e) {
		e.preventDefault();
		$this = $(e.currentTarget);
		if($prompter[0].paused) {
			$prompter[0].play();
		} else {
			$prompter[0].pause();
		}
		$('span.glyphicon', $this).toggleClass('glyphicon-play').toggleClass('glyphicon-pause');
	});
	
	$('form#register:eq(0)').on('submit', function(e) {
		if($('#tosPp:checked').size() == 0) {
			e.preventDefault();
			alert('Please read the Terms of Service before continuing');
		}
	})
	
	$('#recordAnother').on('click', function(e) {
		e.preventDefault();
		modal = $('.doneRecording');
		modal.modal('hide');
		zRecorder.reset();
	});
	
	$('#readyToRecord').on('click', function(e) {
		e.preventDefault();
		modal = $('.beforeRecording');
		modal.modal('hide');
		$prompter[0].play();
		$('#bottom .controls li.play button span.glyphicon').toggleClass('glyphicon-play').toggleClass('glyphicon-pause');
	});
	
//	$('#top .controls li.record button').on('click', function(e) {
//		e.preventDefault();
//		$this = $(e.currentTarget);
//		console.log('clicked record', $('span[class*=record]', $this).size());
//		if($('span[class*=record]', $this).size()) {
////			window.audio.startRecording();
////			window.recorder.startRecording();
////			$prompter[0].play();
//			$('span[class*=record]', $this).toggleClass('glyphicon-record').toggleClass('glyphicon-stop');
//		} else if($('span[class*=stop]', $this).size()) {
////			window.audio.stopRecording();
////			window.recorder.stopRecording();
////			$prompter[0].pause();
//			$('span[class*=stop]', $this).toggleClass('glyphicon-record').toggleClass('glyphicon-stop');
////			window.audio.getDataURL(function(audioDataURL) {
////				window.recorder.getDataURL(function(videoDataURL) {
////					alert('VIDEO SAVED');
////				});
////			});
//		}
//	});
	
	ZiggeoApi.Events.on("recording", function (data) {
		if(selectedVideo != null) {
			$prompter[0].play();
		} else {
			alert('Please select a video before continuing');
		}
	});
	
	ZiggeoApi.Events.on("submitted", function (data) {
		modal = $('.doneRecording');
		modal.modal({
			'backdrop':'static',
			keyboard: false
		});
	});
	
	function _updateView(state) {
		$container.removeClass().addClass('container').addClass('state-' + state);
		switch(state) {
		case 'login':
			modal = $('.loginOrRegister');
			modal.modal({
				'backdrop':'static',
				keyboard: false
			});
			break;
		case 'ready':
			modal = $('.beforeRecording');
			modal.modal();
			for(i=0;i<videos[user.gender].length;i++) {
				//$videosList.append('<li><a href="#" class="btn btn-default btn-sm btn-block"><span class="glyphicon ">&nbsp;</span></a></li>');
				$videosList.append('<tr><td><a href="#"><span class="glyphicon glyphicon-play">&nbsp;</span></a></td></tr>')
				$('tr:last', $videosList)
					.find('a')
						.data('url', videos[user.gender][i].url)
						.on('click', function(e) {
							e.preventDefault();
							$recorder.css('visibility', 'visible');
							$('a', $videosList).removeClass('active');
							$prompter.removeAttr('loop');
							$target = $(e.currentTarget);
							$target.addClass('active');
							$prompter.attr('src',  $target.data('url'));
							selectedVideo = $target.data('url');
							$('#bottom .controls li.play button span.glyphicon').removeClass('glyphicon-play').addClass('glyphicon-pause');
							$prompter[0].play();
						})
						.end()
					.find('span').after(videos[user.gender][i].title)
				;
			}
			
//			navigator.getUserMedia({
//				audio: true,
//				video: true
//			}, function(stream) {
//				$recorder.attr('src', window.URL.createObjectURL(stream));
//				$recorder[0].play();
//				window.audio = RecordRTC(stream);
//				window.recorder = RecordRTC(stream, {type:'video'});
//			}, function(e) {
//				console.error('ERROR', e);
//			});
			break;
		}
	}
	return {
		updateView: _updateView
	}
};
$(document).ready(function() {
	// events
//	$('#login').ajaxForm(function() {
//		console.debug('VLC::', 'login form submitted');
//	});
//	$('#register form').ajaxForm({
//		success: function() {
//			console.debug('VLC::', 'register form submitted', arguments);
//		}
//	});
//	$('#register form').on('submit', function(e) {
//		e.preventDefault();
//		$('#register form').ajaxSubmit();
//	});
//	
//	$('#register form').on('submit', function(e) {
//		e.preventDefault();
//		console.debug('VC::', 'Form Submit', {
//				'username':$('#username').val(),
//				'password':$('passowrd').val(),
//				'email':$('#email').val(),
//				'tosPp':$('#tosPp').val()
//			});
//		$.post(
//			'/auth/local/register',
//			{
//				'username':$('#username').val(),
//				'password':$('passowrd').val(),
//				'email':$('#email').val(),
//				'tosPp':$('#tosPp').val()
//			},
//			function() {
//				console.debug('VC::', 'form submit success', arguments);
//			}
//		);
//	});
	if(typeof window.user == 'undefined' || !window.user.hasOwnProperty('username')) {
		var user = null;
	} else {
		user = window.user;
	}
	var state, states = ['login','ready'], vc = new VirtualChoir();
	if(user == null) {
		state = states[states.indexOf('login')];
	} else {
		state = states[states.indexOf('ready')];
	}
	if(location.pathname == '/'){
		vc.updateView(state);
	}
});