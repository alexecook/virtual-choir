var User = {
  // Enforce model schema in the case of schemaless databases
  schema: true,

  attributes: {
    username  : { type: 'string', unique: true, required: true },
    email     : { type: 'email',  unique: true, required: true },
    tosPp: {type:'string', defaultsTo: 'true'},
    gender: {type:'string', defaultsTo:'female'},
    passports : { collection: 'Passport', via: 'user' },
    videos: {collection: 'Videos', via: 'user'}
  }
};

module.exports = User;
